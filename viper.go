package configmanager

import (
	"fmt"
	"log"
	"net"
	"strings"
	"sync"

	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"
)

type viperWrapper struct {
	ViperEnabled       bool
	Mutex              sync.Mutex
	Restart            chan bool
	configFileNotFound bool
	viper              *viper.Viper
	CfgFile            string
}

func (c *Conf) startViper() {
	c.viper.SetEnvPrefix(c.AppName)

	if c.CfgFile != "" {
		c.viper.SetConfigFile(c.CfgFile)
	} else {
		if c.ConfigurationFileName == "" {
			c.viper.SetConfigName("config")
			c.ConfigurationFileName = "config"
		} else {
			c.viper.SetConfigName(c.ConfigurationFileName)
		}
		c.viper.AddConfigPath("/etc/" + c.AppName + "/")
		c.viper.AddConfigPath("$HOME/." + c.AppName)
		c.viper.AddConfigPath(".")

	}
	//replace "_" in env variable names with . environment variables have the format consts.AppName_Component_Setting in BLOCK CAPITALS
	replacer := strings.NewReplacer(".", "_")
	//pass in replacer to viper so subkeys can be read
	c.viper.SetEnvKeyReplacer(replacer)
	//read in environment variables.
	c.viper.AutomaticEnv()
	c.Restart = make(chan bool, 100)
	if err := c.viper.ReadInConfig(); err != nil {
		if err != err.(viper.ConfigFileNotFoundError) {
			log.Print(err)
		}
		if err == err.(viper.ConfigFileNotFoundError) {
			c.configFileNotFound = true
		}
	}
	err := c.updateConfiguration()
	if err != nil {
		log.Print(err)
	}
	if !c.configFileNotFound {
		c.viper.WatchConfig()
		c.viper.OnConfigChange(func(e fsnotify.Event) {
			fmt.Println("Config file changed:", e.Name)
			if err := c.viper.ReadInConfig(); err != nil {
				if err != err.(viper.ConfigFileNotFoundError) {
					log.Print(err)
				}
				if err == err.(viper.ConfigFileNotFoundError) {
					c.configFileNotFound = true
				}
			}
			err := c.updateConfiguration()
			if err != nil {
				log.Print(err)
			}
			c.Restart <- true
		})
	}
}
func (c *Conf) updateConfiguration() error {
	c.Mutex.Lock()
	if err := c.viper.Unmarshal(c.Configuration); err != nil {
		return err
	}
	//fmt.Print(v.AllSettings())
	c.Mutex.Unlock()
	return nil
}
func (c *Conf) setViperDefault(i confitem) {
	if !i.disableViperDefault {
		switch i.v.(type) {
		case *bool:
			c.viper.SetDefault(i.mapstructureKeyName, *i.v.(*bool))
		case *[]bool:
			c.viper.SetDefault(i.mapstructureKeyName, *i.v.(*[]bool))
		case *string:
			c.viper.SetDefault(i.mapstructureKeyName, *i.v.(*string))
		case *int:
			c.viper.SetDefault(i.mapstructureKeyName, *i.v.(*int))
		case *int32:
			c.viper.SetDefault(i.mapstructureKeyName, *i.v.(*int32))
		case *int64:
			c.viper.SetDefault(i.mapstructureKeyName, *i.v.(*int64))
		case *float32:
			c.viper.SetDefault(i.mapstructureKeyName, *i.v.(*float32))
		case *float64:
			c.viper.SetDefault(i.mapstructureKeyName, *i.v.(*float64))
		case *net.IP:
			c.viper.SetDefault(i.mapstructureKeyName, *i.v.(*net.IP))
		case *net.IPMask:
			c.viper.SetDefault(i.mapstructureKeyName, *i.v.(*net.IPMask))
		case *net.IPNet:
			c.viper.SetDefault(i.mapstructureKeyName, *i.v.(*net.IPNet))
		case *uint8:
			c.viper.SetDefault(i.mapstructureKeyName, *i.v.(*uint8))
		case *uint16:
			c.viper.SetDefault(i.mapstructureKeyName, *i.v.(*uint16))
		case *uint32:
			c.viper.SetDefault(i.mapstructureKeyName, *i.v.(*uint32))
		case *uint64:
			c.viper.SetDefault(i.mapstructureKeyName, *i.v.(*uint64))
		case *uint:
			c.viper.SetDefault(i.mapstructureKeyName, *i.v.(*uint))
		default:
			fmt.Printf("%T\n", i.v)
		}
	}
}
