FROM golang:alpine as gobuilder
ENV CGO_ENABLED=0
COPY . /go/src/gitlab.com/beckett25/go-conf
RUN cd /go/src/gitlab.com/beckett25/go-conf && \
    apk add --no-cache git && \
    go get ./...
RUN cd /go/src/gitlab.com/beckett25/go-conf && \
    go build -ldflags='-s -w' -v -o /go/bin/go-conf .