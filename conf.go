package configmanager

type Conf struct {
	Configuration         interface{}
	AppName               string
	AppVersion            string
	Debug                 bool
	ConfigurationFileName string
	viperWrapper
	cobraWrapper
}

func (c *Conf) init() {
	c = new(Conf)
}
