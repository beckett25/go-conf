package configmanager

import (
	"fmt"
	"log"
	"reflect"

	"github.com/spf13/cobra"

	"github.com/spf13/viper"
)

const mapstructureTagName = "mapstructure"
const disableViperDefaultTagName = "disableViperDefault"
const disableCobraTagName = "disableCobra"
const cobraRequiredTagName = "cobraRequired"
const cobraDescriptionTagName = "cobraDescription"
const cobraNameTagName = "cobraName"
const cobraDisablePersistentFlagName = "cobraDisablePersistentFlag"
const cobraShortNameFlagName = "cobraShortNameFlagName"

type confitem struct {
	mapstructureKeyName string
	viperConfItem
	cobraConfItem
	v interface{}
}
type confitems []confitem
type viperConfItem struct {
	disableViperDefault bool
}
type cobraConfItem struct {
	disableCobra               bool
	cobraName                  string //override either the mapstructure or stuct item name for cobra flag
	cobraRequired              bool   //get cobra to mark item as required
	cobraDescription           string //description of the value
	cobraDepreciated           bool   //get cobra to mark command as depreciated
	cobraDisablePersistentFlag bool   //is command available to all or just the  root command
	cobraShortName             string //set the short cobraflag
	kind                       reflect.Kind
}

//Bootstrap Set Configuration from cobra and or viper
func (c *Conf) Bootstrap() error {
	if c.Configuration == nil {
		return fmt.Errorf("Error: %s", "A Configuration Struct Must be provided")
	}
	var confitems confitems
	confitems, err := processConfItem(c.Configuration)
	if err != nil {
		return err
	}
	//always initialise viper use it for the pflagbinding to struct from cobra
	c.viper = viper.New()
	for i, v := range confitems {
		if c.Debug {
			log.Printf("%d. value: (%v), tag: '%v'\n", i+1, v.v, v.mapstructureKeyName)
		}
		if c.ViperEnabled {
			c.setViperDefault(v)
		}
		if c.CobraEnabled && !v.disableCobra {
			c.setupAdditionalCMD(v)
		}
	}
	if c.ViperEnabled && c.CobraEnabled {
		if c.AppName == "" {
			return fmt.Errorf("%s", "AppName Must be provided")
		}
		if c.AppFunction == nil {
			return fmt.Errorf("%s", "AppFunction Must be provided if Cobra is enabled")
		}
		cobra.OnInitialize(c.startViper)
		c.setupCobra()
	} else if c.ViperEnabled == true {
		if c.AppName == "" {
			return fmt.Errorf("%s", "AppName Must be provided")
		}
		c.startViper()
		if c.AppFunction != nil {
			c.AppFunction()
		}
	} else if c.CobraEnabled == true {
		if c.AppName == "" {
			return fmt.Errorf("%s", "AppName Must be provided")
		}
		if c.AppFunction == nil {
			return fmt.Errorf("%s", "AppFunction Must be provided if Cobra is enabled")
		}
		cobra.OnInitialize(func() { c.updateConfiguration() })
		c.setupCobra()
	}
	return nil
}
