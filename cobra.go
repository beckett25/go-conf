package configmanager

import (
	"fmt"
	"net"

	"github.com/spf13/cobra"
)

type cobraWrapper struct {
	CobraEnabled       bool
	DisableSuggestions bool
	AppFunction        func()
	cobra.Command
}

func (c *Conf) setupCobra() {
	c.Command.Use = c.AppName
	if c.AppVersion != "" && c.Command.Version == "" {
		c.Command.Version = c.AppVersion
	}
	if c.ViperEnabled {
		c.Command.PersistentFlags().StringVar(&c.viperWrapper.CfgFile, "config", "", "config file (default is /etc/"+c.AppName+".yaml, $HOME/."+c.AppName+")")
		c.Command.MarkPersistentFlagFilename("config")
	}
	c.Command.Run = func(cmd *cobra.Command, args []string) {
		c.AppFunction()
	}
	c.Command.Execute()
}

func (c *Conf) setupAdditionalCMD(i confitem) {
	if !i.disableCobra {
		switch i.v.(type) {
		case *bool:
			if !i.cobraDisablePersistentFlag {
				c.Command.PersistentFlags().BoolP(i.cobraName, i.cobraShortName, *i.v.(*bool), i.cobraDescription)
				c.viper.BindPFlag(i.mapstructureKeyName, c.Command.PersistentFlags().Lookup(i.cobraName))
			}
		case *[]bool:
			if !i.cobraDisablePersistentFlag {
				c.Command.PersistentFlags().BoolSliceP(i.cobraName, i.cobraShortName, *i.v.(*[]bool), i.cobraDescription)
				c.viper.BindPFlag(i.mapstructureKeyName, c.Command.PersistentFlags().Lookup(i.cobraName))
			}
		case *string:
			if !i.cobraDisablePersistentFlag {
				c.Command.PersistentFlags().StringP(i.cobraName, i.cobraShortName, *i.v.(*string), i.cobraDescription)
				c.viper.BindPFlag(i.mapstructureKeyName, c.Command.PersistentFlags().Lookup(i.cobraName))
			}
		case *int:
			if !i.cobraDisablePersistentFlag {
				c.Command.PersistentFlags().IntP(i.cobraName, i.cobraShortName, *i.v.(*int), i.cobraDescription)
				c.viper.BindPFlag(i.mapstructureKeyName, c.Command.PersistentFlags().Lookup(i.cobraName))
			}
		case *int32:
			if !i.cobraDisablePersistentFlag {
				c.Command.PersistentFlags().Int32P(i.cobraName, i.cobraShortName, *i.v.(*int32), i.cobraDescription)
				c.viper.BindPFlag(i.mapstructureKeyName, c.Command.PersistentFlags().Lookup(i.cobraName))
			}
		case *int64:
			if !i.cobraDisablePersistentFlag {
				c.Command.PersistentFlags().Int64P(i.cobraName, i.cobraShortName, *i.v.(*int64), i.cobraDescription)
				c.viper.BindPFlag(i.mapstructureKeyName, c.Command.PersistentFlags().Lookup(i.cobraName))
			}
		case *float32:
			if !i.cobraDisablePersistentFlag {
				c.Command.PersistentFlags().Float32P(i.cobraName, i.cobraShortName, *i.v.(*float32), i.cobraDescription)
				c.viper.BindPFlag(i.mapstructureKeyName, c.Command.PersistentFlags().Lookup(i.cobraName))
			}
		case *float64:
			if !i.cobraDisablePersistentFlag {
				c.Command.PersistentFlags().Float64P(i.cobraName, i.cobraShortName, *i.v.(*float64), i.cobraDescription)
				c.viper.BindPFlag(i.mapstructureKeyName, c.Command.PersistentFlags().Lookup(i.cobraName))
			}
		case *net.IP:
			if !i.cobraDisablePersistentFlag {
				c.Command.PersistentFlags().IPP(i.cobraName, i.cobraShortName, *i.v.(*net.IP), i.cobraDescription)
				c.viper.BindPFlag(i.mapstructureKeyName, c.Command.PersistentFlags().Lookup(i.cobraName))
			}
		case *net.IPMask:
			if !i.cobraDisablePersistentFlag {
				c.Command.PersistentFlags().IPMaskP(i.cobraName, i.cobraShortName, *i.v.(*net.IPMask), i.cobraDescription)
				c.viper.BindPFlag(i.mapstructureKeyName, c.Command.PersistentFlags().Lookup(i.cobraName))
			}
		case *net.IPNet:
			if !i.cobraDisablePersistentFlag {
				c.Command.PersistentFlags().IPNetP(i.cobraName, i.cobraShortName, *i.v.(*net.IPNet), i.cobraDescription)
				c.viper.BindPFlag(i.mapstructureKeyName, c.Command.PersistentFlags().Lookup(i.cobraName))
			}
		case *uint8:
			if !i.cobraDisablePersistentFlag {
				c.Command.PersistentFlags().Uint8P(i.cobraName, i.cobraShortName, *i.v.(*uint8), i.cobraDescription)
				c.viper.BindPFlag(i.mapstructureKeyName, c.Command.PersistentFlags().Lookup(i.cobraName))
			}
		case *uint16:
			if !i.cobraDisablePersistentFlag {
				c.Command.PersistentFlags().Uint16P(i.cobraName, i.cobraShortName, *i.v.(*uint16), i.cobraDescription)
				c.viper.BindPFlag(i.mapstructureKeyName, c.Command.PersistentFlags().Lookup(i.cobraName))
			}
		case *uint32:
			if !i.cobraDisablePersistentFlag {
				c.Command.PersistentFlags().Uint32P(i.cobraName, i.cobraShortName, *i.v.(*uint32), i.cobraDescription)
				c.viper.BindPFlag(i.mapstructureKeyName, c.Command.PersistentFlags().Lookup(i.cobraName))
			}
		case *uint64:
			if !i.cobraDisablePersistentFlag {
				c.Command.PersistentFlags().Uint64P(i.cobraName, i.cobraShortName, *i.v.(*uint64), i.cobraDescription)
				c.viper.BindPFlag(i.mapstructureKeyName, c.Command.PersistentFlags().Lookup(i.cobraName))
			}
		case *uint:
			if !i.cobraDisablePersistentFlag {
				c.Command.PersistentFlags().UintP(i.cobraName, i.cobraShortName, *i.v.(*uint), i.cobraDescription)
				c.viper.BindPFlag(i.mapstructureKeyName, c.Command.PersistentFlags().Lookup(i.cobraName))
			}
		default:
			fmt.Printf("%T\n", i.v)
		}
		if i.cobraRequired {
			if !i.cobraDisablePersistentFlag {
				c.Command.MarkFlagRequired(i.cobraName)
			}
		}

	}
}
