package configmanager

import (
	"fmt"
	"reflect"
	"strconv"
	"strings"
)

func processConfItem(s interface{}) (confitems, error) {
	var confitems confitems
	dataVal := reflect.ValueOf(s)
	if dataVal.Kind() == reflect.Ptr {
		dataVal = dataVal.Elem()
		//	iv = reflect.ValueOf(t)
	}
	typ := dataVal.Type()
	for i := 0; i < typ.NumField(); i++ {
		// Get the StructField first since this is a cheap operation. If the
		// field is unexported, then ignore it.
		f := typ.Field(i)
		if f.PkgPath != "" {
			continue
		}

		v := dataVal.Field(i)
		mapstructureTagValue := f.Tag.Get(mapstructureTagName)
		mapstructureTagParts := strings.Split(mapstructureTagValue, ",")
		disableCobraTagValue := f.Tag.Get(disableCobraTagName)
		disableViperDefaultTagValue := f.Tag.Get(disableViperDefaultTagName)
		cobraNameTagValue := f.Tag.Get(cobraNameTagName)
		cobraRequiredTagValue := f.Tag.Get(cobraRequiredTagName)
		cobraDescriptionTagValue := f.Tag.Get(cobraDescriptionTagName)
		disableCobraPersistentFlagValue := f.Tag.Get(cobraDisablePersistentFlagName)
		cobraShortNameFlagValue := f.Tag.Get(cobraShortNameFlagName)
		// Determine the name of the key in the map
		mapstructureKeyName := f.Name
		if mapstructureTagParts[0] != "" {
			if mapstructureTagParts[0] == "-" {
				continue
			}
			mapstructureKeyName = mapstructureTagParts[0]
		}
		if cobraNameTagValue == "" {
			cobraNameTagValue = mapstructureKeyName
		}
		if disableCobraTagValue == "" {
			disableCobraTagValue = "false"
		}
		if disableViperDefaultTagValue == "" {
			disableViperDefaultTagValue = "false"
		}
		if disableCobraPersistentFlagValue == "" {
			disableCobraPersistentFlagValue = "false"
		}
		if cobraRequiredTagValue == "" {
			cobraRequiredTagValue = "false"
		}
		// If "squash" is specified in the tag, we squash the field down.
		squash := false
		for _, tag := range mapstructureTagParts[1:] {
			if tag == "squash" {
				squash = true
				break
			}
		}
		if squash && v.Kind() != reflect.Struct {
			return nil, fmt.Errorf("cannot squash non-struct type '%s'", v.Type())
		}

		switch v.Kind() {
		// this is an embedded struct, so handle it differently
		case reflect.Struct:
			x := reflect.New(v.Type())
			x.Elem().Set(v)

			if squash {
				squashedconfitems, err := processConfItem(x.Interface())
				if err != nil {
					return nil, err
				}
				confitems = append(confitems, squashedconfitems...)
			} else {
				nestedconfitems, err := processConfItem(x.Interface())
				if err != nil {
					return nil, err
				}
				for i := range nestedconfitems {
					nestedconfitems[i].mapstructureKeyName = mapstructureKeyName + "." + nestedconfitems[i].mapstructureKeyName
					nestedconfitems[i].cobraName = cobraNameTagValue + "." + nestedconfitems[i].cobraName
				}
				confitems = append(confitems, nestedconfitems...)
			}

		default:
			var confitem confitem
			var err error
			confitem.disableViperDefault, err = strconv.ParseBool(disableViperDefaultTagValue)
			if err != nil {
				return nil, err
			}
			confitem.disableCobra, err = strconv.ParseBool(disableCobraTagValue)
			if err != nil {
				return nil, err
			}
			confitem.cobraDisablePersistentFlag, err = strconv.ParseBool(disableCobraPersistentFlagValue)
			if err != nil {
				return nil, err
			}
			x := reflect.New(v.Type())
			x.Elem().Set(v)
			confitem.disableCobra, err = strconv.ParseBool(disableCobraTagValue)
			if err != nil {
				return nil, err
			}
			confitem.cobraName = cobraNameTagValue
			confitem.cobraShortName = cobraShortNameFlagValue
			confitem.cobraDescription = cobraDescriptionTagValue
			confitem.cobraRequired, err = strconv.ParseBool(cobraRequiredTagValue)
			if err != nil {
				return nil, err
			}
			confitem.mapstructureKeyName = mapstructureKeyName
			confitem.v = x.Interface()
			confitems = append(confitems, confitem)
		}
	}
	return confitems, nil
}
